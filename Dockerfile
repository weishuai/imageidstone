FROM golang

RUN apt-get update && apt-get install -y \
	gcc \
	g++ \
	libpng-dev \
	libjpeg-dev \
	libfftw3-dev \
	imagemagick \
	make \
	unzip \
	&& apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

ENV CIMG_VERSION 1.6.5
RUN wget http://cimg.eu/files/CImg_$CIMG_VERSION.zip \
	&& unzip CImg_$CIMG_VERSION.zip \
	&& cp CImg-$CIMG_VERSION/CImg.h / \
	&& rm CImg_$CIMG_VERSION.zip \
	&& rm -rf CImg-$CIMG_VERSION

ENV IMAGEID_DIR $GOPATH/src/bitbucket.org/taringa/imageid

COPY . $IMAGEID_DIR
RUN mv /CImg.h $IMAGEID_DIR/cmd/imhash

RUN cd $IMAGEID_DIR && go get -v ./...
RUN cd $IMAGEID_DIR && go install -v cmd/...

CMD ["imageid-server"]

EXPOSE 8080
