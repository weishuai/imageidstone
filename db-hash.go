package imageid

import (
	"bitbucket.org/taringa/imageid/imhash"
	"bitbucket.org/taringa/imageid/kvstore"
	"bitbucket.org/taringa/imageid/log"
	"bitbucket.org/taringa/imageid/stats"
)

const hashMaxDist = 8

func hashFromKey(key string) imhash.ImageHash {
	hash, err := imhash.FromString(key)
	log.OnErrorPanic(err)
	return hash
}

func keyFromHash(hash imhash.ImageHash) string {
	return hash.String()
}

func (db *DB) addHash(tx kvstore.Tx, hash imhash.ImageHash, canonical string) error {
	depth := db.hashTree.Add(hash)
	stats.HashAdded(depth)
	return db.Store.Add(tx, TableHashes, keyFromHash(hash), canonical)
}

func (db *DB) getCanonicalForHash(hash imhash.ImageHash) string {
	canonical, err := db.Store.Get(TableHashes, keyFromHash(hash))
	log.OnErrorPanic(err)
	return canonical
}

func (db *DB) queryHash(url string, hash imhash.ImageHash) (bool, imhash.ImageHash, string, int) {
	found, closest, dist, nodesVisited := db.hashTree.SearchClosest(hash, hashMaxDist)
	stats.TreeQueried(nodesVisited)
	if !found {
		return found, hash, url, dist
	}
	return found, closest, db.getCanonicalForHash(closest), dist
}
