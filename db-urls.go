package imageid

import (
	"bitbucket.org/taringa/imageid/kvstore"
	"bitbucket.org/taringa/imageid/log"
	"bitbucket.org/taringa/imageid/stats"
	"crypto/md5"
	"io"
)

func urlKey(s string) string {
	h := md5.New()
	io.WriteString(h, s)
	return keyFromMd5(h.Sum(nil))
}

func (db *DB) addURL(tx kvstore.Tx, url string, canonical string) error {
	stats.URLEntryAdded(1)
	return db.Store.Add(tx, TableURLs, urlKey(url), canonical)
}

func (db *DB) getCanonicalForURL(url string) string {
	canonical, err := db.Store.Get(TableURLs, urlKey(url))
	log.OnErrorPanic(err)
	return canonical
}
