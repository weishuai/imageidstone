package imageid

import (
	"bitbucket.org/taringa/imageid/log"
	"os"
	"os/signal"
	"syscall"
)

var Shutdown = make(chan int)

func ShuttingDown() bool {
	select {
	case <-Shutdown:
		return true
	default:
	}
	return false
}

func init() {
	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGINT)
	signal.Notify(c, syscall.SIGTERM)
	go func() {
		<-c
		log.Infof("得到信号, 正在关闭...")
		close(Shutdown)
	}()
}
