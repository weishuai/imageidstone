package server

import (
	"bitbucket.org/taringa/imageid"
	"container/ring"
	"encoding/json"
	"net/http"
	"sync"
)

func similarImagesHandler() (http.HandlerFunc, chan *imageid.URLPair) {
	var mu sync.Mutex

	pairs := ring.New(10)
	similarChan := make(chan *imageid.URLPair)

	go func() {
		for p := range similarChan {
			func() {
				mu.Lock()
				defer mu.Unlock()

				pairs = pairs.Next()
				pairs.Value = p
			}()
		}
	}()

	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var slice []*imageid.URLPair
		func() {
			mu.Lock()
			defer mu.Unlock()
			pairs.Do(func(v interface{}) {
				switch p := v.(type) {
				case *imageid.URLPair:
					slice = append([](*imageid.URLPair){p}, slice...)
				}
			})
		}()

		js, err := json.Marshal(slice)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.Write(js)
	})

	return handler, similarChan
}
