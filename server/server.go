package server

import (
	"bitbucket.org/taringa/imageid"
	"bitbucket.org/taringa/imageid/log"
	"github.com/icub3d/graceful"
	"net/http"
	_ "net/http/pprof" // Memory + CPU profiler
	"time"
)

/*
RunServer runs an HTTP server providing the following endpoints:

GET /canonical?url=<URL>: Returns the canonical URL corresponding to url.
If the image url is unknown, an empty string is returned.

POST /process?url=<URL>: Enqueues the url for processing.

GET /debug/vars: Stats (memory, amount of processed images, etc)

GET /debug/similar: Lists the 10 latest similar images found.
*/
func RunServer(reader func(chan<- string)) {
	db := imageid.OpenDB()
	defer db.Close()

	similarHandler, similarChan := similarImagesHandler()

	http.HandleFunc("/canonical", canonicalHandler(db))
	http.HandleFunc("/debug/similar", similarHandler)

	port := ":8080"
	server := graceful.NewServer(&http.Server{
		Addr:         port,
		ReadTimeout:  30 * time.Second,
		WriteTimeout: 30 * time.Second,
	})

	go func() {
		log.Infof("HTTP server listening at port " + port)
		err := server.ListenAndServe()
		// this check is necessary because graceful returns a "use of closed network connection" when Close() is called
		if !imageid.ShuttingDown() {
			log.OnErrorPanic(err)
		}
	}()

	done := make(chan int)
	go func() {
		defer close(done)
		db.Init()
		urlsQueue := imageid.ReadUrls(reader)
		http.HandleFunc("/process", processURLHandler(urlsQueue))
		imageid.ProcessUrls(db, urlsQueue, similarChan)
	}()

	<-imageid.Shutdown
	close(similarChan)
	<-done
	server.Close()
}
