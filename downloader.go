package imageid

import (
	"bitbucket.org/taringa/imageid/config"
	"bitbucket.org/taringa/imageid/log"
	"bitbucket.org/taringa/imageid/stats"
	"io"
	"io/ioutil"
	"net"
	"net/http"
	"os"
	"runtime"
	"strconv"
	"sync"
	"time"
)

type imageQueueItem struct {
	url                string
	downloadedFilename string
}

func copyToTempFile(body io.Reader) (string, error) {
	dir := config.Env("IMAGEID_TMPDIR", os.TempDir())
	file, err := ioutil.TempFile(dir, "imageid")
	if err != nil {
		return "", err
	}
	defer file.Close()

	_, err = io.Copy(file, body)
	if err != nil {
		defer os.Remove(file.Name())
		return "", err
	}
	return file.Name(), nil
}

var client http.Client
var processWokers int
var downloadWorkers int

func init() {
	processWokers = config.EnvInt("IMAGEID_PROCESS_WORKERS", runtime.NumCPU())
	downloadWorkers = config.EnvInt("IMAGEID_DOWNLOAD_WORKERS", 10)

	client = http.Client{
		Transport: &http.Transport{
			Dial: (&net.Dialer{
				Timeout:   3 * time.Second,
				KeepAlive: 10 * time.Second,
			}).Dial,
			TLSHandshakeTimeout: 10 * time.Second,
			DisableKeepAlives:   true,
			MaxIdleConnsPerHost: downloadWorkers,
		},
		Timeout: 120 * time.Second,
	}
}

func downloadURL(workerID string, db *DB, url string, imagesQueue chan<- imageQueueItem) {
	defer log.OnPanicLog(url)
	stats.SetWorker(workerID, "Querying: "+url)
	canonical := db.getCanonicalForURL(url)
	if canonical != "" {
		log.Debugf("[%s] Already indexed. Canonical: %s", url, canonical)
		return
	}

	stats.SetWorker(workerID, "Downloading: "+url)
	resp, err := client.Get(url)
	if log.OnErrorLog(url, err) {
		return
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		log.Errorf("[%s] StatusCode: %d", url, resp.StatusCode)
		return
	}

	filename, err := copyToTempFile(resp.Body)
	if log.OnErrorLog(url, err) {
		return
	}

	stats.SetWorker(workerID, "Blocked (imagesQueue<-) "+url)
	select {
	case imagesQueue <- imageQueueItem{url, filename}:
	case <-Shutdown:
	}
}

func downloadUrls(
	workerID string,
	db *DB, wg *sync.WaitGroup,
	urlsQueue <-chan string,
	imagesQueue chan<- imageQueueItem,
) {
	defer wg.Done()
	defer stats.SetWorker(workerID, "Shutting down")
	for {
		stats.SetWorker(workerID, "Idle (<-urlsQueue)")
		select {
		case url, open := <-urlsQueue:
			if !open {
				return
			}
			downloadURL(workerID, db, url, imagesQueue)
		case <-Shutdown:
			return
		}
	}
}

func processImages(workerID string, db *DB, wg *sync.WaitGroup, imagesQueue <-chan imageQueueItem, similarChan chan<- *URLPair) {
	defer wg.Done()
	defer stats.SetWorker(workerID, "Shutting down")
	for {
		stats.SetWorker(workerID, "Idle (<-imagesQueue)")
		select {
		case item, open := <-imagesQueue:
			if !open {
				return
			}
			func() {
				defer os.Remove(item.downloadedFilename)
				defer log.OnPanicLog(item.url)
				Process(db, workerID, item.downloadedFilename, item.url, similarChan)
			}()
		case <-Shutdown:
			return
		}
	}
}

func ReadUrls(reader func(chan<- string)) chan string {
	urlsQueue := make(chan string, 1000)
	stats.RegisterChan("urls", func() interface{} { return len(urlsQueue) })

	go func() {
		if reader != nil {
			reader(urlsQueue)
		} else {
			defer close(urlsQueue)
			<-Shutdown
		}
	}()

	return urlsQueue
}

func ProcessUrls(
	db *DB,
	urlsQueue <-chan string,
	similarChan chan<- *URLPair,
) {
	imagesQueue := make(chan imageQueueItem, downloadWorkers)
	stats.RegisterChan("images", func() interface{} { return len(imagesQueue) })

	var downloadWg sync.WaitGroup
	for i := 0; i < cap(imagesQueue); i++ {
		downloadWg.Add(1)
		go downloadUrls("downloader"+strconv.Itoa(i), db, &downloadWg, urlsQueue, imagesQueue)
	}

	var processWg sync.WaitGroup
	for i := 0; i < processWokers; i++ {
		processWg.Add(1)
		go processImages("processor"+strconv.Itoa(i), db, &processWg, imagesQueue, similarChan)
	}

	downloadWg.Wait()
	close(imagesQueue)
	processWg.Wait()
}
