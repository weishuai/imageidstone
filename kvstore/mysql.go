package kvstore

import (
	"bitbucket.org/taringa/imageid/log"
	"bitbucket.org/taringa/imageid/stats"
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql" // load "mysql" driver
)

type MysqlStore struct {
	db *sql.DB
}

type MysqlTx struct {
	*sql.Tx
	ok bool
}

func txOk(tx Tx) bool {
	if tx == nil {
		return true
	}
	mtx := tx.(*MysqlTx)
	return mtx.ok
}

func MysqlStoreOpen(tables ...string) *MysqlStore {
	db, err := sql.Open("mysql", "imageid:imageid@tcp(mysql:3306)/imageid")
	log.OnErrorPanic(err)
	for _, table := range tables {
		createKvTable(db, table)
	}

	_, err = db.Exec(`CREATE TABLE IF NOT EXISTS similar_log (
		url VARCHAR(2048) NOT NULL,
		canonical VARCHAR(2048) NOT NULL,
		INDEX canonical (canonical)
	) ENGINE=innodb`)
	log.OnErrorPanic(err)

	return &MysqlStore{db}
}

func createKvTable(db *sql.DB, name string) {
	_, err := db.Exec(`CREATE TABLE IF NOT EXISTS ` + name + ` (
		k char(32) PRIMARY KEY,
		v VARCHAR(2048) NOT NULL
	) ENGINE=innodb`)
	log.OnErrorPanic(err)
}

func (ms *MysqlStore) Close() {
	ms.db.Close()
}

func (ms *MysqlStore) Get(table string, k string) (string, error) {
	var value string
	err := ms.db.QueryRow("SELECT v FROM "+table+" WHERE k = ?", k).Scan(&value)
	if err == sql.ErrNoRows {
		return "", nil
	}
	return value, err
}

func check(tx Tx, err error) bool {
	if err != nil {
		if tx != nil {
			tx.(*MysqlTx).ok = false
		}
		return true
	}
	return false
}

func (ms *MysqlStore) Add(tx Tx, table string, k string, v string) error {
	if !txOk(tx) {
		return fmt.Errorf("Tx not ok")
	}

	if len(k) > 32 {
		return fmt.Errorf("k must be at most 32 bytes long")
	}

	query := "INSERT INTO " + table + " VALUES (?, ?)"

	var err error
	var stmt *sql.Stmt
	if tx == nil {
		stmt, err = ms.db.Prepare(query)
	} else {
		stmt, err = ms.db.Prepare(query)
	}
	if check(tx, err) {
		return err
	}
	defer stmt.Close()
	_, err = stmt.Exec(k, v)
	if check(tx, err) {
		return err
	}
	return nil
}

type MysqlIterator struct {
	rows *sql.Rows
}

func (ms *MysqlStore) GetAllKeys(table string) KeyValueIterator {
	rows, err := ms.db.Query("SELECT k FROM " + table)
	log.OnErrorPanic(err)
	return &MysqlIterator{rows}
}

func (iter *MysqlIterator) Close() {
	iter.rows.Close()
}

func (iter *MysqlIterator) Next() bool {
	return iter.rows.Next()
}

func (iter *MysqlIterator) Current() string {
	var k string
	log.OnErrorPanic(iter.rows.Scan(&k))
	return k
}

func (ms *MysqlStore) Len(table string) int64 {
	var value int64
	err := ms.db.QueryRow("SELECT count(1) FROM " + table).Scan(&value)
	log.OnErrorPanic(err)
	return value
}

func (tx *MysqlTx) End() {
	if tx.ok {
		if log.OnErrorLog("mysql", tx.Commit()) {
			tx.ok = false
		}
	} else {
		log.OnErrorLog("mysql", tx.Rollback())
	}
	stats.TxEnd(tx.ok)
}

func (ms *MysqlStore) StartTx() Tx {
	tx, err := ms.db.Begin()
	if log.OnErrorLog("mysql", err) {
		return nil
	}
	return &MysqlTx{tx, true}
}

func (ms *MysqlStore) LogSimilar(url string, canonical string) error {
	_, err := ms.db.Exec("INSERT INTO similar_log VALUES (?, ?)", url, canonical)
	return err
}
