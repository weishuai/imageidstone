#define cimg_display 0
#define cimg_use_png 1
#define cimg_use_jpeg 1
#include "CImg.h"
#include "imhash.h"

using namespace cimg_library;

typedef unsigned long long ulong64;
typedef unsigned char uint8_t;

static ulong64 dhash(const CImg<uint8_t> &src) {
	const int hash_size = 8;
	CImg<uint8_t> img = src.get_resize(hash_size + 1, hash_size, 1, 1, 6);

	ulong64 hash = 0;

	for (int y = 0; y < hash_size; y++) {
		for (int x = 0; x < hash_size; x++) {
			uint8_t left = img(x, y);
			uint8_t right = img(x + 1, y);
			hash <<= 1;
			hash |= (left < right) ? 1 : 0;
		}
	}

	return hash;
}

/** This function is shamelessly stolen from phash: http://www.phash.org/
 * Copyright (C) 2009 Aetilius, Inc.
 */
static CImg<float>* ph_dct_matrix(const int N) {
	CImg<float> *ptr_matrix = new CImg<float>(N,N,1,1,1/sqrt((float)N));
	const float c1 = sqrt(2.0/N);
	for (int x=0;x<N;x++){
		for (int y=1;y<N;y++){
			*ptr_matrix->data(x,y) = c1*cos((cimg::PI/2/N)*y*(2*x+1));
		}
	}
	return ptr_matrix;
}

/** This function is shamelessly stolen from phash: http://www.phash.org/
 * Copyright (C) 2009 Aetilius, Inc.
 */
static ulong64 phash(const CImg<uint8_t> &src){
	CImg<float> meanfilter(7,7,1,1,1);
	CImg<float> img = src.get_convolve(meanfilter);
	img.resize(32,32);
	CImg<float> *C = ph_dct_matrix(32);
	CImg<float> Ctransp = C->get_transpose();
	CImg<float> dctImage = (*C)*img*Ctransp;
	CImg<float> subsec = dctImage.crop(1,1,8,8).unroll('x');;
	float median = subsec.median();
	ulong64 one = 0x0000000000000001;
	ulong64 hash = 0x0000000000000000;
	for (int i=0;i< 64;i++){
		float current = subsec(i);
		if (current > median)
		hash |= one;
		one = one << 1;
	}
	delete C;
	return hash;
}

static bool image_size_ok(const CImg<uint8_t> &src) {
	return src.width() * src.height() >= 32 * 32;
}

static void crop(CImg<uint8_t> &src) {
	int tries = 10;
	while (--tries > 0) {
		int w = src.width();
		int h = src.height();

		uint8_t color = src(0, 0);
		src = src.autocrop(color);
		if (src.width() > 0 && src.height() > 0) {
			color = src(w - 1, h - 1);
			src = src.autocrop(color);
		}
		if (src.width() == w && src.height() == h) {
			return;
		}
		w = src.width();
		h = src.height();
		if (w == 0 || h == 0) {
			return;
		}
	}
}

static CImg<uint8_t> grayscale(CImg<uint8_t> &src) {
	CImg<uint8_t> img;
	if (src.spectrum() == 3) { // RGB
		img = src.RGBtoYCbCr().channel(0);
	} else if (src.spectrum() == 4) {
		int width = src.width();
		int height = src.height();
		int depth = src.depth();
		img = src.crop(
			0,0,0,0,
			width-1,height-1,depth-1,2
		).RGBtoYCbCr().channel(0);
	} else {
		img = src.channel(0);
	}
	return img;
}

enum {
	ERROR_LOADING_IMAGE = 1,
	ERROR_TOO_MANY_SLICES = 2,
	ERROR_IMAGE_TOO_SIMPLE = 3,
};

extern "C" int imhash(const char *file, imhash_t *hash) {
	cimg::exception_mode(0);

	if (!file || !hash) {
		return ERROR_LOADING_IMAGE;
	}

	try {
		CImg<uint8_t> src;
		src.load(file);
		if (src.depth() != 1) {
			fprintf(stderr, "Animated gifs not supported\n");
			return ERROR_TOO_MANY_SLICES;
		}
		CImg<uint8_t> gray = grayscale(src);

		crop(gray);
		if (!image_size_ok(gray)) {
			fprintf(stderr, "Image is too simple\n");
			return ERROR_IMAGE_TOO_SIMPLE;
		}

		hash->dhash = dhash(gray);
		hash->phash = phash(gray);
		return 0;
	} catch (CImgIOException &ex) {
		fprintf(stderr, "File could not be loaded\n");
		return ERROR_LOADING_IMAGE;
	}

	return 0;
}
