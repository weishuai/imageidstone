// imageid-cli allows to process and query images from the command line.
// imageid-commandline 允许处理和查询图形通过命令行的方式
package main

import (
	"bitbucket.org/taringa/imageid"
	"bitbucket.org/taringa/imageid/imhash"
	"bitbucket.org/taringa/imageid/log"
	"bitbucket.org/taringa/imageid/stats"
	"bufio"
	"fmt"
	"os"
	"strings"
)

func readUrlsFromStdin(urls chan<- string) {
	defer close(urls)
	lines := make(chan string)
	go func() {
		defer close(lines)
		reader := bufio.NewScanner(os.Stdin)
		for reader.Scan() {
			select {
			case lines <- reader.Text():
			case <-imageid.Shutdown:
				return
			}
		}
	}()

	for {
		select {
		case line, ok := <-lines:
			if !ok {
				return
			}
			url := strings.Trim(line, " \n")
			if url != "" {
				urls <- url
			}
		case <-imageid.Shutdown:
			return
		}
	}
}

type command struct {
	name string
	args string
	run  func()
}

func usage(commands []command) {
	fmt.Printf("Usage: %s <command> [args...]\n", os.Args[0])
	for _, cmd := range commands {
		fmt.Printf("       %s %s %s\n", os.Args[0], cmd.name, cmd.args)
	}
}

func withDb(f func(*imageid.DB)) func() {
	return func() {
		db := imageid.OpenDB()
		defer db.Close()
		f(db)
	}
}

func calcHash(filename string) (imhash.ImageHash, error) {
	hash, err := imhash.FromFile(filename)
	if log.OnErrorLog(filename, err) {
		return hash, err
	}
	fmt.Printf("%s %s\n", filename, hash)
	return hash, nil
}

func exec(commands []command, cmdname string) bool {
	for _, cmd := range commands {
		if cmd.name == cmdname {
			cmd.run()
			return true
		}
	}
	return false
}

func main() {
	var commands []command

	commands = append(commands, command{
		"process-file",
		"<file> [url]",
		withDb(func(db *imageid.DB) {
			filename := os.Args[2]
			url := filename
			if len(os.Args) > 3 {
				url = os.Args[3]
			}
			db.Init()
			imageid.Process(db, "CLI", filename, url, nil)
		}),
	})
	commands = append(commands, command{
		"process-urls",
		"",
		withDb(func(db *imageid.DB) {
			db.Init()
			imageid.ProcessUrls(
				db,
				imageid.ReadUrls(readUrlsFromStdin),
				nil,
			)
		}),
	})
	commands = append(commands, command{
		"hash-file",
		"<file>",
		func() { calcHash(os.Args[2]) },
	})
	commands = append(commands, command{
		"hash-dist",
		"<file1> <file2>",
		func() {
			h1, err := calcHash(os.Args[2])
			if err != nil {
				return
			}
			h2, err := calcHash(os.Args[3])
			if err != nil {
				return
			}
			fmt.Printf("Distance: %d\n", h1.Distance(h2))
		},
	})
	commands = append(commands, command{
		"stats",
		"",
		func() { fmt.Println(stats.JSON()) },
	})
	commands = append(commands, command{
		"query",
		"<url>",
		withDb(func(db *imageid.DB) {
			fmt.Printf("%s\n", db.Query(os.Args[2]))
		}),
	})

	if len(os.Args) <= 1 || !exec(commands, os.Args[1]) {
		usage(commands)
	}
}
