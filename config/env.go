package config

import (
	"os"
	"strconv"
)

func Env(key string, defaultValue string) string {
	s := os.Getenv(key)
	if s == "" {
		s = defaultValue
	}
	return s
}

func EnvInt(key string, defaultValue int) int {
	s := os.Getenv(key)
	if s == "" {
		return defaultValue
	}
	r, err := strconv.Atoi(s)
	if err != nil {
		panic(err)
	}
	return r
}
